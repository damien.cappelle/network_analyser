import datetime

class CSVAccess:

    def __init__(self):
        pass

    def record(self, **stats):

        print(stats)
        with open('analysis.csv', 'a') as fd:
            for stype in ['up', 'down']:
                stat = stats.get(stype, {})
                inputs = [f"{datetime.datetime.utcnow().strftime('%d/%m/%Y;%H:%M:%S')};{stype};{key};{value}" for key,value in stat.items()]
                if inputs:
                    fd.write("\n".join(inputs) + '\n')

if __name__ == '__main__':
    
    test = CSVAccess()

    test.record(up={'facebook': 10, 'twitter': 1e10})
